#!/usr/bin/env bash

set -ex

export RUSTFLAGS='-Ctarget-feature=+simd128,+nontrapping-fptoint,+mutable-globals,+sign-ext'

mkdir -p ./out
ln -srfT ./assets ./out/assets
cp -pRL ./html/* ./out/

if [[ $1 != '-n' ]]; then
    cargo update --aggressive

    cargo build --release --target wasm32-unknown-unknown

    wasm-bindgen --out-dir ./out/ --target web \
                 ./target/wasm32-unknown-unknown/release/bevy_test.wasm

    wasm-opt --enable-simd --enable-nontrapping-float-to-int \
             --enable-mutable-globals --enable-sign-ext -O4 -c --strip-debug \
             -o ./out/bevy_test_bg.opt.wasm ./out/bevy_test_bg.wasm
    mv ./out/bevy_test_bg.opt.wasm ./out/bevy_test_bg.wasm
fi
