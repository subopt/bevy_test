#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum AppState {
    Load,
    Play,
}
