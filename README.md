# bevy\_test

[**Try it out, here!**](https://subop.github.io/bevy_test/)

## building

Ensure that you have all of the following in your `$PATH`:

- [`bash`][bash]
- [`cargo`](https://rustup.rs/)
- [`wasm-bindgen`](https://crates.io/crates/wasm-bindgen)
- [`wasm-opt`](https://github.com/WebAssembly/binaryen)

Then:

```bash
git clone 'https://codeberg.org/subopt/bevy_test.git'
cd bevy_test
./release.bash
cd out
python -m http.server  # Or any other web server that will correctly serve WASM
                       # with the `application/wasm` MIME type
```

## assets

All images/textures (metal crate, wooden crate, spark animation) are due to
[@Slime](https://slimeplease.artstation.com/)
([🐦](https://twitter.com/Sslime7))!

[`clang.ogg`](./assets/clang.ogg) and [`clang1.ogg`](./assets/clang1.ogg) are
excerpts from `Single clangs.wav` by <b>CGEffex</b>:
<https://freesound.org/s/96215/> ([CC BY
4\.0](https://creativecommons.org/licenses/by/4.0/)).

[`wood_strike.ogg`](./assets/wood_strike.ogg) and
[`wood_strike1.ogg`](./assets/wood_strike1.ogg) and excerpts from `Wood
chopping.wav` by <b>DDT197</b>: <https://freesound.org/s/445782/>
([CC0](https://creativecommons.org/publicdomain/zero/1.0/)).

[`bevy_test.ogg`](./assets/bevy_test.ogg) is an original piece that I wrote
just for this demo lol
([CC0](https://creativecommons.org/publicdomain/zero/1.0/)).

## legal

Public domain, under the terms of the
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).

[bash]: https://en.wikipedia.org/wiki/Bash_(Unix_shell)
